/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Examen {//Abre clase Exámen
    double A;//Coeficiente de x1
    double B;//Coeficiente de y1
    double C;//Coeficiente de xy
    double D;//Coeficiente de y2
    double E;//
    int n;
    
    Examen(){//Abre método por default
        double A;
        double B;
        double C;
        double D;
        double E;
    }//Cierra Método por default
    Examen(int n){
        this.n=n;
    }
    Examen (double n){
        this.A=n;
    }
    Examen(double x, double y){
        this.A=x;
        this.B=y;
    }
    Examen(double x, double xy, double y){
        this.A=x;
        this.C=xy;
        this.B=y;
    }
    Examen(double x, double xy1, double xy2, double y){
        this.A=x;
        this.C=xy1;
        this.E=xy2;
        this.B=y;
    }
    void setX1(double a){//Abre setX1
        this.A=a;
    }//Cierra setX1
    void setY1(double b){//Abre setY1
        this.B=b;
    }//Cierra setY1
    void setXY(double c){//Abre setX2
        this.C=c;
    }//Cierra setX2
    void setY2(double d){//Abre setY2
        this.D=d;
    }//Cierra setY2}
    void setXY2(double e){
        this.E=e;
    }
    double getX1(){//Abre getX1
        return (this.A);
    }//Cierra getX1
    double getY1(){//Abre getY1
        return (this.B);
    }//Cierra getY1
    double getXY(){//Abre getX2
        return(this.C);
    }//Cierra getX2
    double getC(){//Abre getY2
        return(this.D);
    }//Cierra getY2
    double getXY2(){
        return(this.E);
    }
    /**
     * Mostrará el binomio en su forma (Ax+By)
     */
    void show(){//Abre show
        System.out.printf("(%.2fx+%.2fy)%n",getX1(),getY1());
    }//Cierra show
    /**
     * Muestra la forma de la multiplicación de binomios y del binomio al
     * cuadrado desarrollado
     */
    void showM(){
        System.out.printf("(%.2fx^2+%.2fxy+%.2fy^2)%n",getX1(),getXY(),getY1());
    }
    /**
     * Muestra la forma desarrollada del binomio al cubo
     */
    void showBM(){
        System.out.printf("(%.2fx^3+%.2fx^2y+%.2fxy^2+%.2fy^3)%n",getX1(),
                getXY(),getXY2(),getY1());
    }
    /**
     * Muestra la forma factorizada del binomio al cuadrado
     */
    void showB2(){
        System.out.printf("(%.2fx+%.2fy)^2",getX1(),getY1());
    }
    /**
     * ;uestra la forma factorizada del binomio al cubo
     */
    void showB3(){
        System.out.printf("(%.2fx+%.2fy)^3",getX1(),getY1());
    }
    /**
     * Muestra la forma del binomio conjugado
     */
    void showConjugado(){
        System.out.printf("%.2fx^2-%.2fy^2",getX1(),getY1());
    }
    void showFactorial(){
        System.out.printf("%.2f! es: ",getX1());
    }
    void showBinomioNewton(){
        System.out.printf("(%.2fx+%.2fy)^%.2f",getX1(),getY1(),getC());
    }

    /**
     * Va a sumar los binomios que se le dé
     * @param r
     * @return 
     */
    Examen SumaBinomios(Examen r){//Abre suma de Binomios
        double x;
        double y;
        
        x=getX1()+r.getX1();
        y=getY1()+r.getY1();
        return(new Examen(x,y));
    }//Cierra suma de Binomios
    
    Examen RestaBinomios(Examen r){//Abre resta de Binomios
        double x;
        double y;
        
        x=getX1()-r.getX1();
        y=getY1()-r.getY1();
        
        return(new Examen(x,y));
    }//Cierra resta de Binomios
    Examen Multiplicación(Examen r){
        double x;
        double y;
        double xy;
        
        x=getX1()*r.getX1();
        xy=(getX1()*r.getY1())+(getY1()*r.getX1());
        y=getY1()*r.getY1();
        
        return(new Examen(x,xy,y));
    }
    Examen MultiplicarPorC(double C){
        double x;
        double y;
        
        x=getX1()*C;
        y=getY1()*C;
        
        System.out.printf("%.2f(%.2fx+%.2fy)",C,getX1(),getY1());
        return(new Examen(x,y));
    }
    Examen BinomioConjugado(){
        double x;
        double y;
        x=getX1()*getX1();
        y=getY1()*(-getY1());
        System.out.printf("(%.2fx+%.2fy)(%.2fx-%.2fy)",getX1(),getY1(),getX1(),
                getY1());
        
        return(new Examen (x,y));
    }
    Examen BinomioCuadrado(){
        double x;
        double y;
        double xy;
        
        x=getX1()*getX1();
        xy=2*(getX1()*getY1());
        y=getY1()*getY1();
        
        return(new Examen(x,xy,y));
    }
    Examen BinomioCubo(){
        double x;
        double y;
        double xy1;
        double xy2;
        
        x=getX1()*getX1()*getX1();
        xy1=3*(getX1()*getX1())*getY1();
        xy2=3*getX1()*(getY1()*getY1());
        y=getY1()*getY1()*getY1();
        
        return(new Examen(x,xy1,xy2,y));
    }
    //Para calcular el binomio (Ax+By)^n primero es necesario calcular las
    //combinaciones de n en k, pero antes necesitamos saber calcular el factorial
    static int Factorial(int n){
        int F=1;
        for(int i=1; i<=n; i++){//Abre for
            F=F*i;
        }//Cierra for      
        return F;
    }
    public static int Combinaciones(int n, int k){
        
        int C=(Factorial(n))/((Factorial(k))*(Factorial(n-k)));
        return C;
    }
    /**void CombinacionesNenK(int n){
        int m=n;
        int k=0;
        String Combinaciones="";
        while(m!=-1 && k!=(n+1)){
            Combinaciones +=Combinaciones(n,k)+"+";
            m--;
            k++;
        }
        System.out.println(Combinaciones);
    }
    */
    
    String BinomioNewton(int n){
        int x=n;
        int y=0;
        
        String Exp="";
        while(x !=-1 && y != (n+1)){
            if(y == n){
              Exp +=Combinaciones(n,y)+"*("+Math.pow(getX1(),x)+")x^"+x+"*("+
                    Math.pow(getY1(),y)+")y^"+y+" ";  
            }
            else{
            Exp +=Combinaciones(n,y)+"*("+Math.pow(getX1(),x)+")x^"+x+"*("+
                    Math.pow(getY1(),y)+")y^"+y+"+";
            }
            x=x-1;
            y=y+1;
        }
        System.out.println(Exp);
        return(Exp);
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {//Abre main
        // TODO code application logic here
        Scanner s=new Scanner (System.in);
        /**System.out.print("¿Cuál es el valor de n?");
        n=s.nextInt();
        Examen r=new Examen();
        r.CombinacionesNenK(n);
        r.Exponentes(n);
        */
        
        
        double x1, y1, x2, y2, C, n;
        int op=0;
        Examen r1, r2, r3;
        
        
        System.out.println("¿Qué quieres hacer?\n 1) Sumar\n 2) Restar\n "
                + "3) Multiplicar\n 4) Multiplicar por una constante\n "
                + "5) Binomio Conjugado\n 6) Binomio al"
                + " cuadrado\n 7) Binomio al cubo\n 8) Binomio de "
                + "Newton\n 9) Salir\n");
        
        while(op != 9){//Abre while
            
        op=s.nextInt();
        switch(op){//Abre Switch
            
            case 1://Suma
                System.out.println("Sumar:\n Los binomios tienen la forma"
                        + " (Ax+By)\n");
                System.out.print("Da el coeficiente de x del primer"
                        + " binomio: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y del primer"
                        + " binomio: ");
                y1=s.nextDouble();
                System.out.print("Da el coeficiente de x del segundo"
                        + " binomio: ");
                x2=s.nextDouble();
                System.out.print("Da el coeficiente de y del segundo"
                        + " binomio: ");
                y2=s.nextDouble();
                
                r1=new Examen (x1,y1);
                r2=new Examen (x2, y2);
                r3=r1.SumaBinomios(r2);
                
                System.out.print("La suma es: ");
                r3.show();
                
                System.out.println("¿Qué más quieres hacer?\n");
                break;
                
            case 2://Resta
                System.out.println("Restar:\n Los binomios tienen la forma"
                        + " (Ax+By)\n");                
                System.out.print("Da el coeficiente de x del primer"
                        + " binomio: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y del primer"
                        + " binomio: ");
                y1=s.nextDouble();
                System.out.print("Da el coeficiente de x del segundo"
                        + " binomio: ");
                x2=s.nextDouble();
                System.out.print("Da el coeficiente de y del segundo"
                        + " binomio: ");
                y2=s.nextDouble();
                
                r1=new Examen(x1,y1);
                r2=new Examen(x2, y2);
                r3=r1.RestaBinomios(r2);
                
                System.out.print("La suma es: ");
                r3.show();
                System.out.println("¿Qué más quieres hacer?\n");
                
                break;
                
            case 3://Multiplicación
                System.out.println("Multiplicar:\n Los binomios tienen la forma"
                        + " (Ax+By)\n");                
                System.out.print("Da el coeficiente de x del"
                        + " primer Binomio: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y del primer"
                        + " binomio: ");
                y1=s.nextDouble();
                System.out.print("Da el coeficiente de x del segundo"
                        + " binomio: ");
                x2=s.nextDouble();
                System.out.print("Da el coeficiente de y del segundo"
                        + " binomio: ");
                y2=s.nextDouble();
                
                r1=new Examen(x1,y1);
                r2=new Examen(x2,y2);
                r3=r1.Multiplicación(r2);
                
                System.out.print("La multiplicación es: ");
                r3.showM();
                System.out.println("¿Qué más quieres hacer?\n");
                
                break;
            case 4://Multiplicar por una constante
                System.out.println("Multiplicar por una constante:\n Los "
                        + "binomios que son multiplicados por "
                        + "una constante tienen la forma C(Ax+By)");                
                System.out.print("Da el coeficiente de x: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y: ");
                y1=s.nextDouble();
                System.out.print("Da la constante por la que quieras "
                        + "multiplicar el binomio: ");
                C=s.nextDouble();
                
                r1=new Examen(x1,y1);
                r3=r1.MultiplicarPorC(C);
                System.out.print("=");
                r3.show();
                System.out.println("¿Qué más quieres hacer?\n");
                
                break;
                
            case 5://Binomio conjugado
                System.out.println("Binomio conjugado:\n Tienen la forma: "
                        + "((Ax)^2-(By)^2)");              
                System.out.print("Da el coeficiente de x: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y: ");
                y1=s.nextDouble();
                r1=new Examen(x1,y1);
                
                r3=r1.BinomioConjugado();
                System.out.print("=");
                r3.showConjugado();
                System.out.println("\n¿Qué más quieres hacer?\n");
                break;
            
            case 6://Binomio al cuadrado
                System.out.println("Binomio al cuadrado:\n");            
                System.out.print("Da el coeficiente de x: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y: ");
                y1=s.nextDouble();   
                
                r1=new Examen(x1,y1);
                r1.showB2();
                System.out.print("=");
                r3=r1.BinomioCuadrado();
                r3.showM();
                
                System.out.println("¿Qué más quieres hacer?\n");
                break;
                
            case 7://Binomio al cubo
                System.out.println("Binomio al cubo:\n ");             
                System.out.print("Da el coeficiente de x: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y: ");
                y1=s.nextDouble();
                
                r1=new Examen (x1,y1);
                r1.showB3();
                System.out.print("=");
                r3=r1.BinomioCubo();
                r3.showBM();
                
                System.out.println("¿Qué más quieres hacer?\n");
                break;
                
            case 8://Binomio de Newton
                System.out.println("Binomio a la n:\n");                
                System.out.print("Da el coeficiente de x: ");
                x1=s.nextDouble();
                System.out.print("Da el coeficiente de y: ");
                y1=s.nextDouble();
                System.out.print("¿Cuál es el valor de n? ");
                n=s.nextInt();
                
                System.out.print("("+x1+"x+"+y1+"y)^"+n+"= ");
                Examen r=new Examen(x1,y1);
                String b=r.BinomioNewton((int) n);
                
                System.out.println("¿Qué más quieres hacer?\n");
                
                break;
               
            case 9:
                System.out.println("Saliendo...");
                break;
            default: 
                System.out.println("Error: opción no disponible."+"\n"+"¿Qué quieres"
                        + " hacer?");
            break;  
        
        }//Cierra Switch
      
        }//Cierra While
        
    }//cierra main
    
}//Cierra clase Examen
